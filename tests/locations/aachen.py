import unittest
from locations.aachen import (
    retrieve_einbuergerungsbehoerde,
    retrieve_aufenhaltsangelegenheiten_abholung_aufenthaltserlaubnis,
    retrieve_aufenthalt_team1,
    retrieve_aufenthalt_team2,
    retrieve_aufenthalt_team3,
    retrieve_aufenthalt_fiktionsbescheinigung,
    retrieve_aufenthalt_abholung_reiseausweis
)

# simple tests that just call the function. The function
# must fail if there is a non 200 http response. So this
# is a basic smoke test that verifies that the functions
# do execute successful, not a sophisticated unit test
class AachenSmokeTests(unittest.TestCase):
    def test_retrieve_einbuergerungsbehoerde(self):
        retrieve_einbuergerungsbehoerde()
        
    def test_retrieve_aufenhaltsangelegenheiten_abholung_aufenthaltserlaubnis(self):
        retrieve_aufenhaltsangelegenheiten_abholung_aufenthaltserlaubnis()

    def test_retrieve_aufenthalt_team1(self):
        retrieve_aufenthalt_team1()

    def test_retrieve_aufenthalt_team2(self):
        retrieve_aufenthalt_team2()

    def test_retrieve_aufenthalt_team3(self):
        retrieve_aufenthalt_team3()

    def test_retrieve_aufenthalt_fiktionsbescheinigung(self):
        retrieve_aufenthalt_fiktionsbescheinigung()
    
    def test_retrieve_aufenthalt_abholung_reiseausweis(self):
        retrieve_aufenthalt_abholung_reiseausweis()

if __name__ == '__main__':
    unittest.main()