import unittest
import poll_appointments
import freezegun
import datetime

class AachenSmokeTests(unittest.TestCase):
    def test_verify_interval(self):
        with freezegun.freeze_time("2000-01-01 12:00:00"):
            delta = datetime.timedelta(minutes=5)
            self.assertFalse(poll_appointments.time_passed_by(datetime.datetime.now() - delta, delta))
            self.assertFalse(poll_appointments.time_passed_by(datetime.datetime.now(), delta))
            self.assertTrue(poll_appointments.time_passed_by(datetime.datetime.now() - delta - delta, delta))