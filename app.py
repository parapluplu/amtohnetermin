from flask import request, render_template, Blueprint, Flask
import mail
import database
import config
import logging
from typing import Optional

site = Blueprint('site', __name__, template_folder='templates', static_folder='static')

logging.basicConfig(level=logging.INFO)
app = Flask(__name__)

def render_page(success: Optional[str] = None, error: Optional[str] = None) -> None:
    return render_template(
        'index.html',
        options=config.locations['Aachen'].keys(),
        error=error,
        success=success
    )

@app.route('/', methods=['GET'])
def view_index():
    return render_page()

@app.route('/unsubscribe')
def unsubscribe():
    mail = request.args.get('mail')
    for location in config.locations.keys():
        for location_option in config.locations[location].keys():
            database.remove_mail(mail, location, location_option)
    return render_page(success="Successfully unsubscribed")


@app.route('/', methods=['POST'])
def submit_mail():
    mail_address = request.form['mail']
    location_options = request.form.getlist('location_options')
    if len(location_options) == 0:
        return render_page(error='No appointment type selected')
    if mail.validate_mail(mail_address):
        database.add_mail(mail_address, 'Aachen', location_options)
        return render_page(success="Thanks, your mail is registered")
    else:
        return render_page(error="Invalid Email Address")
