import os
import boto3
import logging
from typing import List, Dict
import datetime
from timedelta_isoformat import timedelta
from cachetools import cached, TTLCache

TABLE_NAME = f'emails-{os.environ.get("STAGE", "dev")}'

_dynamodb_client = boto3.client('dynamodb')
_dynamodb_resource = boto3.resource('dynamodb')

def add_locations(location_name: str, location_options: List[str]) -> None:
    for location_option in location_options:
        try:
            _dynamodb_client.put_item(
                TableName=TABLE_NAME,
                Item={
                    'location_name': {'S': location_name},
                    'location_option': {'S': location_option},
                    'last_send': {'S': (datetime.datetime.now() - timedelta(minutes=60)).isoformat()},
                    'send_interval': {'S': timedelta(minutes=60).isoformat()},
                },
                ConditionExpression='attribute_not_exists(location_name)'
            )
            logging.info(f"{location_name} - {location_option} added to database")
        except _dynamodb_resource.meta.client.exceptions.ConditionalCheckFailedException as _:
            # If adding the element fails due to the conditional check that's fine
            logging.info(f"{location_name} - {location_option} exists in database")

def add_mail(mail: str, location_name: str, location_options: List[str]) -> None:
    for location_option in location_options:
        _dynamodb_client.update_item(
            TableName=TABLE_NAME,
            Key={'location_name': {'S': location_name}, 'location_option': {'S': location_option}},
            UpdateExpression="ADD emails :val",
            ExpressionAttributeValues={':val': {'SS': [mail]}}
        )

def remove_mail(mail: str, location_name: str, location_option: str) -> None:
    _dynamodb_client.update_item(
        TableName=TABLE_NAME,
        Key={'location_name': {'S': location_name}, 'location_option': {'S': location_option}},
        UpdateExpression="DELETE emails :val",
        ExpressionAttributeValues={':val': {'SS': [mail]}}
    )

# For lambda executions data may be available between executions. Caching helps to reduce DynamoDB costs
# see https://aws.amazon.com/blogs/compute/container-reuse-in-lambda/
@cached(cache=TTLCache(maxsize=1024, ttl=3600))
def mails_for_location(location_name: str, location_option: str, **kwargs) -> Dict:
    logging.info(f"Retrieve mails for {location_name} with option {location_option} and kwargs {kwargs}")
    table = _dynamodb_resource.Table(TABLE_NAME)
    return table.get_item(
        Key={'location_name': location_name, 'location_option': location_option},
    )

def update_last_send(location_name: str, location_option: str, last_send: datetime.datetime):
    _dynamodb_client.update_item(
        TableName=TABLE_NAME,
        Key={'location_name': {'S': location_name}, 'location_option': {'S': location_option}},
        UpdateExpression="SET last_send = :last_send_val, send_interval = :send_interval_val",
        ExpressionAttributeValues={
                ':last_send_val': {'S': last_send.isoformat()},
                ':send_interval_val': {'S': timedelta(minutes=60).isoformat()}
            }
    )

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    import config
    for location_name in config.locations.keys():
        add_locations(location_name, config.locations[location_name].keys())
