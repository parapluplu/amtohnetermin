from dataclasses import dataclass
import re
import os
import smtplib
import urllib.parse
from email.message import EmailMessage

EMAIL_REGEX = re.compile('[^@]+@[^@]+\\.[^@]+')
MAIL_RECIPIENTS_FILE = os.path.join(os.environ.get('datastore', '.'), "mail.txt")

@dataclass
class MailAccess:
   mail_user: str
   mail_from_address: str
   mail_password: str
   smtp_server: str
   smtp_port: int

def validate_mail(mail) -> bool:
    return EMAIL_REGEX.fullmatch(mail)

def send_mail(appointment_type: str, recipient: str, mail_access: MailAccess) -> None:
  with smtplib.SMTP(mail_access.smtp_server, port=mail_access.smtp_port) as server:
    server.starttls()
    server.ehlo()
    server.login(mail_access.mail_user, mail_access.mail_password)
    msg = EmailMessage()
    msg.set_content(f"Appointment for '{appointment_type}' available. Visit https://termine.staedteregion-aachen.de/auslaenderamt\nGood luck!\n\nUnsubscribe from all mails: https://refresh.guru/unsubscribe?mail={urllib.parse.quote(recipient)}\n")
    msg["From"] = mail_access.mail_from_address
    msg["To"] = recipient
    msg["Subject"] = "Immigration office has appointments available"
    server.send_message(msg)
    server.quit()
