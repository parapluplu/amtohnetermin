import logging
import mail
import database
import config
import datetime
from timedelta_isoformat import timedelta
import boto3
import os
import datetime
import time
from cachetools import cached, TTLCache


def lambda_entry(event, lambda_context):
  do_it()

"""
Returns true if the interval passed by is long enough to allow resending an email
"""
def time_passed_by(last_send: datetime.datetime, send_interval: datetime.timedelta) -> bool:
  return datetime.datetime.now() > (last_send + send_interval)

"""
Returns the mail server access such as user, password, server, port

Calls are cached, to reduce costs to the AWS secrets manager. Cache size
should not exceed 1 as this function has no arguments. But we've set it to 24
so future refactorings of this function, which introduce parameters, don't immediately
fail to not cache anything anymore.
"""
@cached(cache=TTLCache(maxsize=24, ttl=3600))
def get_mail_access() -> mail.MailAccess:
  logging.info("Retrieve mail access data")
  secrets_client = boto3.client('secretsmanager')
  smtp_server = secrets_client.get_secret_value(SecretId="refresh_guru_smtp_server")['SecretString']
  smtp_port = int(secrets_client.get_secret_value(SecretId="refresh_guru_smtp_port")['SecretString'])
  mail_from_address = secrets_client.get_secret_value(SecretId="refresh_guru_mail_from")['SecretString']
  mail_user = secrets_client.get_secret_value(SecretId="refresh_guru_mail_user")['SecretString']
  mail_password = secrets_client.get_secret_value(SecretId="refresh_guru_mail_password")['SecretString']
  return mail.MailAccess(mail_user, mail_from_address, mail_password, smtp_server, smtp_port)

def do_it():
  logging.basicConfig(level=logging.INFO, force=True)
  mail_access = get_mail_access()

  logging.info("go for it")
  # check availability for multiple locations
  for location, location_config in config.locations.items():
    for location_option, poll_method in location_config.items():
      logging.info(f"Poll appointments for {location} option '{location_option}'")
      if poll_method():
        logging.info("Appointments available!")
        item = database.mails_for_location(location, location_option)['Item']
        send_interval = timedelta.fromisoformat(item.get('send_interval', "PT1H"))
        last_send = datetime.datetime.fromisoformat(
          item.get('last_send', "1990-01-01 01:00:00.000000")
        )
        if time_passed_by(last_send, send_interval):
          mail_addresses = item.get('emails', set())
          for mail_address in mail_addresses:
            if mail.validate_mail(mail_address):
              logging.info(f"Send mail to {mail_address}")
              mail.send_mail(location_option, mail_address, mail_access)
              new_last_send = datetime.datetime.now()
              # update the cached item
              item['last_send'] = new_last_send
              database.update_last_send(location, location_option, new_last_send)
            else:
              logging.warning(f"Invalid mail '{mail_address}'")
        else:
          logging.info(f"Not sending mails, as a mail was send {last_send} already. "
                       f"Send interval configured to {send_interval}. Next mail at {last_send + send_interval}.")

if __name__ == "__main__":
  import schedule
  import time
  logging.basicConfig(level=logging.INFO)
  schedule.every(5).seconds.do(do_it)
  while True:
    schedule.run_pending()
    time.sleep(1)
