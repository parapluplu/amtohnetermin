import requests
import logging

def _evaluate_response(response: requests.Response) -> bool:
    if not response.ok:
        logging.error(f"Response returned non ok status code '{response.status_code}' with body: {response.text}")
        return False
    if "Es ist ein Fehler aufgetreten" in response.text:
        logging.error(f"Unexpected message found when evaluating response text: {response.text}")
        return False
    return "Kein freier Termin verfügbar" not in response.text

def retrieve_einbuergerungsbehoerde():
    session = requests.Session()
    # necessary so the staedteregion system assigns a user and desired type of appointment to our session
    session.get("https://termine.staedteregion-aachen.de/auslaenderamt/select2?md=2")
    session.get("https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=90&select_cnc=1&cnc-304=1&cnc-305=0")
    # get the actual page we're interested in now
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = session.post(
        "https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=90&select_cnc=1&cnc-304=1&cnc-305=0",
        data="loc=41&gps_lat=50.770786&gps_long=6.118778&select_location=Ausl%C3%A4nderamt+Aachen+-+Aachen+Arkaden%2C+Trierer+Stra%C3%9Fe+1%2C+Aachen+ausw%C3%A4hlen",
        headers=headers
    )
    return _evaluate_response(response)

def select_aufenthaltsangelegenheiten() -> requests.Session:
    session = requests.Session()
    # necessary so the staedteregion system assigns a user and desired type of appointment to our session
    session.get("https://termine.staedteregion-aachen.de/auslaenderamt/select2?md=1")
    return session

def retrieve_aufenhaltsangelegenheiten_abholung_aufenthaltserlaubnis():
    session = select_aufenthaltsangelegenheiten()
    session.get("https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=89&select_cnc=1&cnc-299=1&cnc-300=0&cnc-293=0&cnc-296=0&cnc-297=0&cnc-301=0&cnc-284=0&cnc-298=0&cnc-291=0&cnc-285=0&cnc-282=0&cnc-283=0&cnc-303=0&cnc-281=0&cnc-287=0&cnc-286=0&cnc-289=0&cnc-292=0&cnc-288=0&cnc-279=0&cnc-280=0&cnc-290=0&cnc-295=0&cnc-294=0")
    # get the actual page we're interested in now
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = session.post(
        "https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=89&select_cnc=1&cnc-299=1&cnc-300=0&cnc-293=0&cnc-296=0&cnc-297=0&cnc-301=0&cnc-284=0&cnc-298=0&cnc-291=0&cnc-285=0&cnc-282=0&cnc-283=0&cnc-303=0&cnc-281=0&cnc-287=0&cnc-286=0&cnc-289=0&cnc-292=0&cnc-288=0&cnc-279=0&cnc-280=0&cnc-290=0&cnc-295=0&cnc-294=0",
        data="loc=41&gps_lat=50.770786&gps_long=6.118778&select_location=Ausl%C3%A4nderamt+Aachen+-+Aachen+Arkaden%2C+Trierer+Stra%C3%9Fe+1%2C+Aachen+ausw%C3%A4hlen",
        headers=headers
    )
    return _evaluate_response(response)

def retrieve_aufenthalt_team1():
    session = select_aufenthaltsangelegenheiten()
    session.get("https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=89&select_cnc=1&cnc-299=0&cnc-300=0&cnc-293=1&cnc-296=0&cnc-297=0&cnc-301=0&cnc-284=0&cnc-298=0&cnc-291=0&cnc-285=0&cnc-282=0&cnc-283=0&cnc-303=0&cnc-281=0&cnc-287=0&cnc-286=0&cnc-289=0&cnc-292=0&cnc-288=0&cnc-279=0&cnc-280=0&cnc-290=0&cnc-295=0&cnc-294=0")
    # get the actual page we're interested in now
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = session.post(
        "https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=89&select_cnc=1&cnc-299=0&cnc-300=0&cnc-293=1&cnc-296=0&cnc-297=0&cnc-301=0&cnc-284=0&cnc-298=0&cnc-291=0&cnc-285=0&cnc-282=0&cnc-283=0&cnc-303=0&cnc-281=0&cnc-287=0&cnc-286=0&cnc-289=0&cnc-292=0&cnc-288=0&cnc-279=0&cnc-280=0&cnc-290=0&cnc-295=0&cnc-294=0",
        data="loc=45&gps_lat=50.768703&gps_long=6.091849&select_location=Ausl%C3%A4nderamt+Aachen%2C+2.+Etage+ausw%C3%A4hlen",
        headers=headers
    )
    return _evaluate_response(response)

def retrieve_aufenthalt_team2():
    session = select_aufenthaltsangelegenheiten()
    session.get("https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=89&select_cnc=1&cnc-299=0&cnc-300=0&cnc-293=0&cnc-296=1&cnc-297=0&cnc-301=0&cnc-284=0&cnc-298=0&cnc-291=0&cnc-285=0&cnc-282=0&cnc-283=0&cnc-303=0&cnc-281=0&cnc-287=0&cnc-286=0&cnc-289=0&cnc-292=0&cnc-288=0&cnc-279=0&cnc-280=0&cnc-290=0&cnc-295=0&cnc-294=0")
    # get the actual page we're interested in now
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = session.post(
        "https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=89&select_cnc=1&cnc-299=0&cnc-300=0&cnc-293=0&cnc-296=1&cnc-297=0&cnc-301=0&cnc-284=0&cnc-298=0&cnc-291=0&cnc-285=0&cnc-282=0&cnc-283=0&cnc-303=0&cnc-281=0&cnc-287=0&cnc-286=0&cnc-289=0&cnc-292=0&cnc-288=0&cnc-279=0&cnc-280=0&cnc-290=0&cnc-295=0&cnc-294=0",
        data="loc=45&gps_lat=50.768703&gps_long=6.091849&select_location=Ausl%C3%A4nderamt+Aachen%2C+2.+Etage+ausw%C3%A4hlen",
        headers=headers
    )
    return _evaluate_response(response)

def retrieve_aufenthalt_team3():
    session = select_aufenthaltsangelegenheiten()
    session.get("https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=89&select_cnc=1&cnc-299=0&cnc-300=0&cnc-293=0&cnc-296=0&cnc-297=1&cnc-301=0&cnc-284=0&cnc-298=0&cnc-291=0&cnc-285=0&cnc-282=0&cnc-283=0&cnc-303=0&cnc-281=0&cnc-287=0&cnc-286=0&cnc-289=0&cnc-292=0&cnc-288=0&cnc-279=0&cnc-280=0&cnc-290=0&cnc-295=0&cnc-294=0")
    # get the actual page we're interested in now
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = session.post(
        "https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=89&select_cnc=1&cnc-299=0&cnc-300=0&cnc-293=0&cnc-296=0&cnc-297=1&cnc-301=0&cnc-284=0&cnc-298=0&cnc-291=0&cnc-285=0&cnc-282=0&cnc-283=0&cnc-303=0&cnc-281=0&cnc-287=0&cnc-286=0&cnc-289=0&cnc-292=0&cnc-288=0&cnc-279=0&cnc-280=0&cnc-290=0&cnc-295=0&cnc-294=0",
        data="loc=45&gps_lat=50.768703&gps_long=6.091849&select_location=Ausl%C3%A4nderamt+Aachen%2C+2.+Etage+ausw%C3%A4hlen",
        headers=headers
    )
    return _evaluate_response(response)

def retrieve_aufenthalt_fiktionsbescheinigung():
    session = select_aufenthaltsangelegenheiten()
    session.get("https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=93&select_cnc=1&cnc-299=0&cnc-300=0&cnc-293=0&cnc-296=0&cnc-297=0&cnc-313=1&cnc-284=0&cnc-307=0&cnc-312=0&cnc-285=0&cnc-310=0&cnc-283=0&cnc-303=0&cnc-309=0&cnc-287=0&cnc-286=0&cnc-289=0&cnc-292=0&cnc-288=0&cnc-279=0&cnc-280=0&cnc-311=0&cnc-295=0&cnc-294=0")
    # get the actual page we're interested in now
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = session.post(
        "https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=93&select_cnc=1&cnc-299=0&cnc-300=0&cnc-293=0&cnc-296=0&cnc-297=0&cnc-313=1&cnc-284=0&cnc-307=0&cnc-312=0&cnc-285=0&cnc-310=0&cnc-283=0&cnc-303=0&cnc-309=0&cnc-287=0&cnc-286=0&cnc-289=0&cnc-292=0&cnc-288=0&cnc-279=0&cnc-280=0&cnc-311=0&cnc-295=0&cnc-294=0",
        data="loc=41&gps_lat=50.770786&gps_long=6.118778&select_location=Ausl%C3%A4nderamt+Aachen+-+Aachen+Arkaden%2C+Trierer+Stra%C3%9Fe+1%2C+Aachen+ausw%C3%A4hlen",
        headers=headers
    )
    return _evaluate_response(response)

def retrieve_aufenthalt_abholung_reiseausweis():
    session = select_aufenthaltsangelegenheiten()
    session.get("https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=89&select_cnc=1&cnc-299=0&cnc-300=0&cnc-293=0&cnc-296=0&cnc-297=0&cnc-301=0&cnc-284=1&cnc-298=0&cnc-291=0&cnc-285=0&cnc-282=0&cnc-283=0&cnc-303=0&cnc-281=0&cnc-287=0&cnc-286=0&cnc-289=0&cnc-292=0&cnc-288=0&cnc-279=0&cnc-280=0&cnc-290=0&cnc-295=0&cnc-294=0")
    # get the actual page we're interested in now
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = session.post(
        "https://termine.staedteregion-aachen.de/auslaenderamt/location?mdt=89&select_cnc=1&cnc-299=0&cnc-300=0&cnc-293=0&cnc-296=0&cnc-297=0&cnc-301=0&cnc-284=1&cnc-298=0&cnc-291=0&cnc-285=0&cnc-282=0&cnc-283=0&cnc-303=0&cnc-281=0&cnc-287=0&cnc-286=0&cnc-289=0&cnc-292=0&cnc-288=0&cnc-279=0&cnc-280=0&cnc-290=0&cnc-295=0&cnc-294=0",
        data="loc=41&gps_lat=50.770786&gps_long=6.118778&select_location=Ausl%C3%A4nderamt+Aachen+-+Aachen+Arkaden%2C+Trierer+Stra%C3%9Fe+1%2C+Aachen+ausw%C3%A4hlen",
        headers=headers
    )
    return _evaluate_response(response)

def not_supported():
    pass

config = {
    "Einbürgerungsbehörde - Abgabe Einbürgerungsantrag/Beratungstermin": retrieve_einbuergerungsbehoerde,
    "Aufenthaltsangelegenheiten - Abholung Aufenthaltserlaubnis": retrieve_aufenhaltsangelegenheiten_abholung_aufenthaltserlaubnis,
    # "Aufenthaltsangelegenheiten - Abholung Reiseausweis": not_supported,
    "Aufenthaltsangelegenheiten - Erteilung/Verlängerung Aufenthalt - Nachname: A - Z (Team 1)": retrieve_aufenthalt_team1,
    "Aufenthaltsangelegenheiten - Erteilung/Verlängerung Aufenthalt - Nachname: A - Z (Team 2)": retrieve_aufenthalt_team2,
    "Aufenthaltsangelegenheiten - Erteilung/Verlängerung Aufenthalt - Nachname: A - Z (Team 3)": retrieve_aufenthalt_team3,
    "Aufenthaltsangelegenheiten - Fiktionsbescheinigung (Erteilung/Verlängerung)": retrieve_aufenthalt_fiktionsbescheinigung,
    # "Aufenthaltsangelegenheiten - Übertrag Aufenthalts-/Niederlassungserlaubnis": not_supported,
    # "Aufenthaltsangelegenheiten - Reiseausweis für Flüchtlinge und Übertrag Niederlassungserlaubnis": not_supported,
    # "Aufenthaltsangelegenheiten - Grenzgängerkarte": not_supported,
    # "Aufenthaltsangelegenheiten - Integrationskurs (Beratung/Verpflichtung)": not_supported,
    # "Aufenthaltsangelegenheiten - Beratung (Einreise, Aufenthaltserlaubnis, Niederlassungserlaubnis)": not_supported,
    # "Aufenthaltsangelegenheiten - Beratung (Gestattung, Duldung, Unionsbürger)": not_supported,
    # "Aufenthaltsangelegenheiten - Verpflichtungserklärung/ Einladung": not_supported,
    # "Aufenthaltsangelegenheiten - Visaverlängerung/ medizinische Behandlung": not_supported,
    # "Aufenthaltsangelegenheiten - Daueraufenthaltsbescheinigung": not_supported,
    # "Aufenthaltsangelegenheiten - RWTH Studenten": not_supported,
    # "Aufenthaltsangelegenheiten - RWTH Familienangehörige": not_supported,
    # "Aufenthaltsangelegenheiten - RWTH Mitarbeitende & Forschende bzw. PhD": not_supported,
    # "Aufenthaltsangelegenheiten - FH Studenten": not_supported,
    # "Aufenthaltsangelegenheiten - FH Familienangehörige": not_supported,
    # "Aufenthaltsangelegenheiten - FH Mitarbeitende & Forschende bzw. PhD ": not_supported,
    # "Aufenthaltsangelegenheiten - Schülersammelliste": not_supported,
    # "Aufenthaltsangelegenheiten - Aufenthaltsgestattung": not_supported,
    # "Aufenthaltsangelegenheiten - Duldung": not_supported,
}